import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class ClosestToZero {

    private static List<Integer> input = new ArrayList<>();

    static void execution() throws Exception {
        Scanner getClosestZero = new Scanner(System.in);
        System.out.println("Enter list of number.\nPress any alphabet to return closes to zero number");
        while (getClosestZero.hasNextInt()) {
            input.add(getClosestZero.nextInt());
        }
        if (input.size() <= 1) {
            System.out.println("Minumum 2 numbers");
            input.clear();
            return;
        }
        int curr = 0;
        int near = input.get(0);
        for (Integer integer : input) {
            curr = integer * integer;
            if (curr <= (near * near)) {
                if ((curr) == (near * near)) {
                    if (integer > 0) {
                        near = integer;
                    }
                } else {
                    near = integer;
                }
            }
        }
        input.clear();
        System.out.println("Closest Number = " + near);
    }
}
