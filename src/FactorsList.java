class FactorsList {
    static void execution() throws Exception {
        System.out.println("Computing first 1500 Natural Numbers");
        int i = 2;
        while (i <= 1500) {
            int t = i;
            StringBuilder data = new StringBuilder(" :");
            while (t % 2 == 0) {
                t = t / 2;
                data.append(" 2");
            }
            while (t % 3 == 0) {
                t = t / 3;
                data.append(" 3");
            }
            while (t % 5 == 0) {
                t = t / 5;
                data.append(" 5");
            }
            if (t == 1) {
                System.out.println(i + data.toString());
            }
            i++;
        }
    }
}
