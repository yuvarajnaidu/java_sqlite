import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

class SumOfTwoLargeNumber {
    private static List<Integer> user_input_number = new ArrayList<>();

    static void execution() throws Exception {
        Scanner getInt = new Scanner(System.in);
        System.out.println("Enter list of number.\nPress any alphabet to computer 2 large sum of number");
        while (getInt.hasNextInt()) {
            user_input_number.add(getInt.nextInt());
        }
        Collections.sort(user_input_number);
        if (user_input_number.size() < 1 || user_input_number.size() == 1) {
            System.out.println("\nNot Enough Of Numbers");
            return;
        } else {
            int answer = user_input_number.get(user_input_number.size() - 1) + user_input_number.get(user_input_number.size() - 2);
            System.out.println("\nSum is " + answer);
            user_input_number.clear();
        }
    }
}
