import java.util.Scanner;

public class MyClass {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String option = "Enter 1 For Question 1\nEnter 2 For Question 2\nEnter 3 For Question 3\n\nSQL\nEnter 4 for SQL Mock Data" +
                "\nEnter 5 for SQL Question 2 " +
                "\nEnter 6 for SQL Question 3 " +
                "\nEnter any alphabet To Exit";
        System.out.println(option);
        while (scanner.hasNextInt()) {
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    SumOfTwoLargeNumber.execution();
                    break;
                case 2:
                    ClosestToZero.execution();
                    break;
                case 3:
                    FactorsList.execution();
                    break;
                case 4: case 5: case 6:
                    SQLQueries.execution(choice);
                    break;
            }
            scanner.reset();
            System.out.println("\n" + option + "\n");
        }
    }
}
