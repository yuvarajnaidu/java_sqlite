import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.sql.*;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

class SQLQueries {

    private static Statement statement;

    static void execution(int choice) {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection(
                    "jdbc:sqlite:greatEastern.db");


            statement = con.createStatement();
            try{
                statement.execute("CREATE TABLE IF NOT EXISTS customerOrder(" +
                        "order_id int NOT NULL PRIMARY KEY," +
                        "customer_name VARCHAR(255) NOT NULL," +
                        "order_date date NOT NULL," +
                        "quantity int NOT NULL );"
                );
            }catch (Exception e){
                e.printStackTrace();

            }


            switch (choice) {
                case 4:
                    mockData();
                    break;
                case 5:
                    quantityLargerThan();
                    break;
                case 6:
                    listDownEachCustomerWithHowManyOrders();
                    break;
                default:
                    break;
            }
            con.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void listDownEachCustomerWithHowManyOrders() throws SQLException {
        String query = "SELECT DISTINCT customer_name FROM customerOrder;";
        Map<String, Integer> dataQuantity = new HashMap<>();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            String customer_name = resultSet.getString("customer_name");

            dataQuantity.put(customer_name, 0);
        }
        dataQuantity.forEach((k, v) -> {
            String getData = "SELECT quantity FROM customerOrder WHERE customer_name='" + k + "';";
            ResultSet quantity = null;
            try {
                quantity = statement.executeQuery(getData);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            int count = 0;
            while (true) {
                try {
                    if (!quantity.next()) break;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                try {
                    count += quantity.getInt("quantity");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            dataQuantity.put(k, count);
        });
        String leftAlignFormat = "| %-15s | %-4d |%n";
        System.out.format("+-----------------+------+%n");
        System.out.format("| Customer name     | Quantity   |%n");
        System.out.format("+-----------------+------+%n");
        dataQuantity.forEach((k, v) -> {
            System.out.format(leftAlignFormat, k, v);
        });
        System.out.format("+-----------------+------+%n");
    }

    private static void mockData() {
        System.out.println("How Many Data want to be inserted?");
        Scanner mockDataCount = new Scanner(System.in);
        int number;
        try {
            number = mockDataCount.nextInt();
        } catch (InputMismatchException e) {
            return;
        }
        if (number < 0) {
            System.out.println("Only Positive Integer Number");
            mockData();
        }
        for (int i = 1; i <= number; i++) {
            LocalDate randomDate = createRandomDate(2018, 2019);
            int random = (int) (Math.random() * (100 - 1));
            String sql = MessageFormat.format("INSERT INTO customerOrder " +
                    "VALUES ({0},{1},{2},{3});\n", i, "'greatEastern_" + random + "'", "'" + randomDate + "'", random);
            try {
                statement.executeUpdate(sql);
                System.out.println(sql);
            } catch (SQLException e) {
                number++;
            }
        }
    }

    private static LocalDate createRandomDate(int startYear, int endYear) {
        int day = createRandomIntBetween(1, 28);
        int month = createRandomIntBetween(1, 12);
        int year = createRandomIntBetween(startYear, endYear);
        return LocalDate.of(year, month, day);
    }

    private static int createRandomIntBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    private static void quantityLargerThan() {
        System.out.println("Enter the number of quantity greater than");
        Scanner scanner = new Scanner(System.in);
        int quantity;
        try {
            quantity = scanner.nextInt();
        } catch (InputMismatchException e) {
            return;
        }
        if (quantity < 0) {
            System.out.println("Only Positive Integer");
        }
        String query = "select * " +
                "from customerOrder where quantity > " + quantity + " ORDER BY order_date DESC ;";
        try {
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                int order_id = resultSet.getInt("order_id");
                String order_date = resultSet.getString("order_date");
                String customer_name = resultSet.getString("customer_name");
                int quantity1 = resultSet.getInt("quantity");
                System.out.println("\nLatest Order : \n" +
                        "Order ID      = " + order_id + "\n" +
                        "Customer Name = " + customer_name + "\n" +
                        "Order Date    = " + order_date + "\n" +
                        "Quantity      = " + quantity1 + "\n"
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
